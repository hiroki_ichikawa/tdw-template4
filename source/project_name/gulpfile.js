/**
 * gulpfile
 * gulp4 required
 *
 * @version 4.0.0
 * @author tdw hi
 *
 * ├── src                  ソース（ここで開発）
 * ├── dev                  ビルド
 * ├── package.json
 * ├── gulpfile.js
 * └── gulp
 *      ├── gulp.config.js
 *      ├── gulp.util.js
 *      └── tasks
 *          ├── server.js    サーバ
 *          ├── sass.js      sass
 *          ├── scripts.js   es/js
 *          ├── pug.js       pug
 *          ├── html.js      html
 *          ├── image.js     画像
 *          └── deploy.js    デプロイ
 *
 * srcディレクトリで開発してください
 * gulp default でファイルのウォッチが始まり
 * devディレクトリにビルドされていきます
 * browserSyncもdevディレクトリを見ています
 * gulp deploy するとdev内のものがhtdocsにデプロイされます
 * 設定はgulp/gulp.config.jsに
 *
 * esのトランスパイルは、polyfillのrequireを行いません
 * polyfillの埋め込みが必要なESは別途webpackなりでコンパイルしてください
 *
 */

const config = require('./gulp/gulp.config.js')
	,util = require('./gulp/gulp.util.js')
;

const gulp = require('gulp')
	requireDir = require('require-dir')
;

requireDir('./gulp/tasks');



/**
 * build
 */
gulp.task('build'
	,gulp.series(
		// init
		gulp.parallel(
			'sass_compile',
			'pug_compile',
			'html_copy',
			'es_transpile',
			'image_minify'
		),
		(done) => {
			done();
		}
	)
);



/**
 * default
 */
gulp.task('default'
	,gulp.series(
		// init
		gulp.parallel(
			'browser_sync',
			'build'
		),
		// watch
		gulp.parallel(
			'browser_sync_watch',
			'sass_watch',
			'pug_watch',
			'html_watch',
			'es_watch',
			'image_watch'
		),
		(done) => {
			done();
		}
	)
);




/**
 * deploy
 */
gulp.task('deploy'
	,gulp.series(
		'css_minify',
		'js_minify',
		'text_replace',
		'text_encoding',
		'file_copy',
		(done) => {
			done();
		}
	)
);
