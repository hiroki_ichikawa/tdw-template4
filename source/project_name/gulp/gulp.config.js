/**
 * gulp config
 */

module.exports = {

	// directory
	path: {
		src: './src',
		dev: './dev',
		deploy: '../../htdocs',
		ignore_prefix: [
			'__'
		],
		ignore_suffix: [
			'__'
		]
	},

	server: {
		https: false
	},

	// image
	image: {
		is_minify: true,
		png: [0.7, 0.85],
		jpg: 85,
		gif: 180
	},

	// deploy
	deploy: {
		encoding: {
			html: 'UTF-8',
			css: 'UTF-8',
			js: 'UTF-8'
		},
		minify: {
			html: false,
			css: false,
			js: false
		},
		is_sass: false, // .sassファイルを含めるか
		is_cssmap: false, // .css.mapファイルを含めるか
		is_jsmap: false // .js.mapファイルを含めるか
	}
};
