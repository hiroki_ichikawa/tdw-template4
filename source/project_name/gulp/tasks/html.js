/**
 * gulp task
 * html他、何もする必要のないファイルをコピーするだけのタスク
 *
 *
 */

const config = require('../gulp.config.js')
	,util = require('../gulp.util.js')
;

const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
;

/**
 * copy html and others
 * @return {object} gulp
 */
const html_copy = () => {

	let src_file = [
		path.join(config.path.src, '**/*'),
		'!' + path.join(config.path.src, '**/*.{jpg,jpeg,png,svg,gif}'),
		'!' + path.join(config.path.src, '**/*.pug'),
		'!' + path.join(config.path.src, '**/*.scss'),
		'!' + path.join(config.path.src, '**/*.es6'),
	];

	let dest_dir = config.path.dev;

	return gulp
		.src(src_file, {
			cwd: './',
			base: config.path.src
		})
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>") //<-
		}))
		.pipe(gulp.dest(dest_dir, {
			cwd: './'
		}))
	;
}
module.exports.html_copy = html_copy;
gulp.task('html_copy', html_copy);


/**
 * watch html and others
 * @return {object} gulp
 */
const html_watch = () => {

	// let src_file = [
	// 	path.join(config.path.src, '**/*'),
	// 	'!' + path.join(config.path.src, '**/*.{jpg,jpeg,png,svg,gif}'),
	// 	'!' + path.join(config.path.src, '**/*.pug'),
	// 	'!' + path.join(config.path.src, '**/*.scss'),
	// 	'!' + path.join(config.path.src, '**/*.es6'),
	// ];
	let src_file = [
		config.path.src + '/**/*',
		'!' + config.path.src + '/**/*.{jpg,jpeg,png,svg,gif}',
		'!' + config.path.src + '/**/*.pug',
		'!' + config.path.src + '/**/*.scss',
		'!' + config.path.src + '/**/*.es6',
	];
	return gulp.watch(src_file, gulp.series('html_copy'));
}
module.exports.html_watch = html_watch;
gulp.task('html_watch', html_watch);
