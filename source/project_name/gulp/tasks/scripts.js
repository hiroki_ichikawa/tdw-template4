/**
 * gulp task
 * js
 */

const config = require('../gulp.config.js')
	,util = require('../gulp.util.js')
;


const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
	babel = require('gulp-babel')
;

/**
 * transpile script
 * @return {object} gulp
 */
const es_transpile = () => {

	let src_file = path.join(config.path.src, '**/*.es6');
	let dest_dir = config.path.dev;

	return gulp
		.src(src_file, {
			cwd: './',
			base: config.path.src,
			since: gulp.lastRun(es_transpile),
			sourcemaps: true
		})
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>") //<-
		}))
		.pipe(babel())
		.pipe(gulp.dest(dest_dir, {
			cwd: './',
			sourcemaps: '.'
		}))
	;
}
module.exports.es_transpile = es_transpile;
gulp.task('es_transpile', es_transpile);


/**
 * watch script
 * @return {object} gulp
 */
const es_watch = () => {

	// let src_file = path.join(config.path.src, '**/*.es6');
	let src_file = config.path.src + '/**/*.es6';
	return gulp.watch(src_file, gulp.series('es_transpile'));
}
module.exports.es_watch = es_watch;
gulp.task('es_watch', es_watch);
