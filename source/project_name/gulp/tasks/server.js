/**
 * gulp task
 * http server
 *
 * config.path.devフォルダをDocumentRootとします
 * proxy先のWebサーバもそのように設定してください
 */

const config = require('../gulp.config.js')
	,server = require('../gulp.server.js')
	,util = require('../gulp.util.js')
;

const gulp = require('gulp')
	path = require('path')
	plumber = require('gulp-plumber')
	notify = require('gulp-notify')
	browserSync = require('browser-sync')
;


/**
 * browser_sync
 * @param  {Function} callback コールバック
 * @return {Object}            gulp
 */
const browser_sync = (callback) => {
	browserSync.init({
		proxy: server.proxy,
		port: server.port,
		open: 'external',
		reloadDelay: 200,
		notify: false,
		ghostMode: false,
		https: config.server.https
	});

	callback();
}
module.exports.browser_sync = browser_sync;
gulp.task('browser_sync', browser_sync);


/**
 * browser_sync_reload
 * @param  {Function} callback コールバック
 * @return {Object}            gulp
 */
const browser_sync_reload = (callback) => {
	browserSync.reload();
	callback();
}
module.exports.browser_sync_reload = browser_sync_reload;
gulp.task('browser_sync_reload', browser_sync_reload);


/**
 * browser_sync_watch
 * @return {Object}            gulp
 */
const browser_sync_watch = (callback) => {
	// let src_file = path.join(config.path.dev, '**/*');
	let src_file = config.path.dev + '/**/*';
	return gulp.watch(src_file, gulp.series('browser_sync_reload'));
}
module.exports.browser_sync_watch = browser_sync_watch;
gulp.task('browser_sync_watch', browser_sync_watch);
