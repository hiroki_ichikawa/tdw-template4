
class myClass {
	constructor(){
		this.name = 'digital works';
		this.age = 3;
	}

	get age (){
		return this.age - 0;
	}

	set age(age){
		this.age = parseInt(age);
	}

	method1(){
		var obj = {
			foo: 123,
			var: 456,
			baz: 789
		};

		for(let key of Object.keys(obj)){
			console.log(key, obj[key]);
		}
	}
}
