

var SAMPLE = window.SAMPLE || {};

SAMPLE.common = SAMPLE.common || {

	/**
	 * レスポンシブ関連
	 * @type {Object}
	 */
	responsive : {
		type : null,
		threshold : 768, // ブレイクポイント
		vp_default : '',
		clientClass : [],

		/**
		 * 組み立て
		 * @return {[type]} [description]
		 */
		build : function(){
			var A = this;

			this.vp_default = $('meta[name=viewport]').attr('content');

			this.setClientClassName();

			$(window).on('resize orientationchange',function(){
				A.detect();
			});
			this.detect();
		},

		/**
		 * PC/SPの判別
		 */
		detect : function(){
			var A = this;
			var ww = window.innerWidth;
			var type = null;

			if(ww > this.threshold){
				type = 'pc';
			}
			else{
				type = 'sp';
			}

			if(type != this.type){
				this.type = type;
				$('html').removeClass('pc sp').addClass(type);

				if(
					this.clientClass.indexOf('ipad') != -1 ||
					this.clientClass.indexOf('android-tablet') != -1
				){
					if(type == 'pc'){
						$('meta[name=viewport]').attr({ content : 'width=1300' });
					}
					else{
						$('meta[name=viewport]').attr({ content : this.vp_default });
					}
				}
			}
		},

		/**
		 * UA判定
		 */
		setClientClassName : function(){
			var ua = navigator.userAgent.toLowerCase();
			var cn = [];

			if(/iphone|ipod|ipad/.test(ua)){
				cn.push('ios');
				if(ua.indexOf('iphone') != -1) cn.push('iphone');
				else if(ua.indexOf('ipod') != -1) cn.push('ipod');
				else if(ua.indexOf('ipad') != -1) cn.push('ipad');

				ua.replace(/os (\d)_/,function($0,$1){
					cn.push('ios' + $1);
				});

				if(ua.indexOf('safari') != -1) cn.push('safari');
			}
			else if(ua.indexOf('android') != -1){

				cn.push('android');
				if(ua.indexOf('chrome') != -1) cn.push('android-chrome');
				else if(ua.indexOf('firefox') != -1) cn.push('android-firefox');
				else cn.push('android-default');

				if(ua.indexOf('mobile') == -1) cn.push('android-tablet');
				else cn.push('android-mobile');
			}
			else if(ua.indexOf('os x') != -1){
				cn.push('osx');
			}
			else if(ua.indexOf('windows') != -1){
				cn.push('windows');
			}

			if(ua.indexOf('msie') != -1){
				cn.push('msie');
				ua.replace(/msie (\d+)/,function($0,$1){
					cn.push('msie-' + $1);
				});
			}

			if(ua.indexOf('trident') != -1){
				cn.push('trident');
			}
			else if(ua.indexOf('edge') != -1){
				cn.push('edge');
			}
			else if(ua.indexOf('safari') != -1){
				if(ua.indexOf('chrome') != -1) cn.push('chrome');
				else{
					cn.push('safari');
					if(ua.indexOf('version/5') != -1) cn.push('safari-5')
				}
			}
			else{
				ua.replace(/(chrome|firefox|opera)/,function($0,$1){
					cn.push($1);
				})
			}

			this.clientClass = cn;

			var html = $('html');
			$(cn).each(function(){
				html.addClass(this.toString());
			});

		}
	}
};
SAMPLE.common.responsive.build();



SAMPLE.common.createCarousel = function(opt){
	opt = opt || {};

	var carousel = {
		div : null,
		wrap : null,
		stage : null,
		ul : null,
		uls : [],
		lis : null,
		indi_prev : null,
		indi_next : null,
		indi_wrap : null,
		indi_ol : null,
		swipe_target : null,

		article_class : [],
		article_length : 0,
		indi_article : [],

		is_fullsize : false,
		is_hoverstop : false,
		is_centering : false,
		is_silent : false,
		is_swipe : true,
		is_stable : false,
		max_width : 99999,
		min_width : 0,

		step_width_fn : null,

		step_width : 0,
		indi_step_width : 0,
		indi_wrap_width : 0,
		ul_width : 0,
		centering_offset : 0,
		stage_left : 0,

		current : 0,
		current_div : null,

		available : true,
		indi_available : true,
		timer_id : null,
		auto_interval : 5000,
		duration_k : 0.5,
		easing : {
			label : 'easeOut',
			bezier : 'cubic-bezier(.2,0,.25,1)'
		},

		is_move : false,
		touch_offset_x : 0,
		touch_dx : 0,

		/**
		 * 組み立て
		 * @param  {Object} opt 各種オプション
		 */
		build : function(opt){
			var A = this;
			opt = opt || {};

			this.is_fullsize = (opt.is_fullsize === true) ? true : false ;
			this.is_hoverstop = (opt.is_hoverstop === true) ? true : false ;
			this.is_indicator = (opt.is_indicator === true) ? true : false ;
			this.is_centering = (opt.is_centering === true) ? true : false ;
			this.is_silent = (opt.is_silent === true) ? true : false ;
			this.is_swipe = (opt.is_swipe === false) ? false : true ;
			this.is_stable = (opt.is_stable === true) ? true : false ;
			if(!!opt.auto_interval) this.auto_interval = opt.auto_interval;
			if(!!opt.max_width) this.max_width = opt.max_width;
			if(!!opt.min_width) this.min_width = opt.min_width;
			if(!!opt.duration_k) this.duration_k = opt.duration_k;
			if(!!opt.step_width_fn) this.step_width_fn = opt.step_width_fn;
			if(!!opt.swipe_target) this.swipe_target = opt.swipe_target;
			if(!!opt.easing && !!opt.easing.label) this.easing.label = opt.easing.label;
			if(!!opt.easing && !!opt.easing.bezier) this.easing.bezier = opt.easing.bezier;


			this.setVars();

			this.div = $(opt.div).off();
			this.wrap = opt.wrap || $('.wrap',this.div);
			this.stage = opt.stage || $('.stage',this.wrap).off();
			this.ul = opt.ul || $('ul',this.wrap);
			this.indi_prev = opt.indi_prev || $('.prev',this.div);
			this.indi_next = opt.indi_next || $('.next',this.div);
			if(!!this.is_indicator){
				this.indi_ol = opt.indi_ol || $('ol.indicator',this.div);
				this.indi_wrap = opt.ind_wrap || this.indi_ol.parent();
			}

			this.lis = (!!opt.li_selector) ? $(opt.li_selector.toString(),this.ul) : $('li',this.ul);

			this.article_length = this.lis.length;
			this.step_width = this.lis.eq(0).width()
				+ parseInt(this.lis.eq(0).css('margin-left'),10)
				+ parseInt(this.lis.eq(0).css('margin-right'),10);

			if(this.is_fullsize){
				this.step_width = this.div.width();
				var mw = (typeof this.min_width == 'function') ? this.min_width() : this.min_width;
				if(mw > this.step_width) this.step_width = mw;
			}

			this.ul_width = this.step_width * this.article_length;
			this.ul.css({ width : this.ul_width });

			this.lis.each(function(){
				var li = $(this);
				A.article_class.push(li.attr('class'));
			});

			this.uls.push(this.ul);
			for(var i=0 ; i<2 ; i++)(function(index){
				var cul = A.ul.clone(true);
				A.uls.push(cul);
				A.stage.append(cul);
			})(i);

			// this.stage.append(this.ul.clone(true));
			// this.stage.append(this.ul.clone(true));
			this.stage_left = 0 - this.ul_width;
			this.stage.css({ width : this.ul_width * 3 });


			if(!!this.is_indicator){
				for(var i=0 ; i<this.article_length ; i++)(function(index){
					var li = $('<li><a href="#">' + index + '</a></li>').appendTo(A.indi_ol);
					var obj = A.createIndiArticle(index,li);
					if(A.is_indicator){
						A.indi_article.push(obj);
					}
				})(i);

				this.showIndicator();

			}



			// event関連

			$(window).on('resize',function(){
				A.adjust();
			});
			this.adjust();


			if(!!this.indi_prev) this.indi_prev.on(this.CLICK_EVENT_NAME,function(evt){
				evt.preventDefault();
				if(!A.available) return;
				if(!A.indi_available) return;
				A.step(-1);
			});

			if(!!this.indi_next) this.indi_next.on(this.CLICK_EVENT_NAME,function(evt){
				evt.preventDefault();
				if(!A.available) return;
				if(!A.indi_available) return;
				A.step(1);
			});


			if(!!this.is_swipe){
				var s_target = (!!this.swipe_target) ? $(this.swipe_target) : this.stage;

				s_target.on('touchstart',function(evt){
					if(!!A.is_move) return false;
					A.available = false;
					A.autoStop();

					A.touch_offset_x = evt.originalEvent.touches[0].clientX;
				});

				s_target.on('touchend',function(evt){
					if(!!A.is_move) return false;
					A.flickFix();
					A.autoStart();
				});

				s_target.on('touchmove',function(evt){
					if(!!A.is_move) return false;
					var touchX = evt.originalEvent.touches[0].clientX;
					A.touch_dx = touchX - A.touch_offset_x;
					// evt.preventDefault();
					A.flickMove();
				});
			}

			if(!!this.is_hoverstop){
				this.div.hover(function(){ A.autoStop(); },function(){ A.autoStart(); });
				this.div.on('touchstart',function(){ A.autoStop(); });
				this.div.on('touchend',function(){ A.autoStart(); });
			}



			// 初期実行
			this.adjust();
			this.activateArticle();
			this.autoStart();


		},

		/**
		 * フリック時
		 */
		flickMove : function(){
			var A = this;

			// 横成分はフリック
			this.stage.css({ transform : this.CSS_TRANSLATE_VALUE(this.stage_left + this.touch_dx + this.centering_offset) });

			// 縦成分はスクロールに
			// window.scrollTo(0,this.win_scroll_top - this.touch_dy);
		},

		/**
		 * フリック終了時
		 */
		flickFix : function(){
			var A = this;
			this.is_move = true;

			var restart_fn = function(){
				A.is_move = false;
				A.available = true;
				A.touch_dx = 0;
				A.stage.css({ transition : '' });;
				A.autoStart();
			};

			// touchmoveしていないときはなにもしない
			if(this.touch_dx == 0){
				restart_fn();
			}
			// しきい値以上動いた場合はstepする
			else if(Math.abs(this.touch_dx) > this.step_width / 6){
				this.step(
					(this.touch_dx > 0) ? -1 : 1,
					function(){
						restart_fn();
					},
					// 動きをスムーズにするためイージングをeaseOut系に
					'cubic-bezier(0.215, 0.61, 0.355, 1)'
				);
			}
			// しきい値以下の場合は戻す
			else{
				var teFn = function(){
					A.stage.off(A.EVENT_TRANSITION_END,teFn);
					restart_fn();
				};
				this.stage.on(this.EVENT_TRANSITION_END,teFn);
				this.stage.css({ transition : this.CSS_TRANSITION_VALUE(0.5) });
				this.stage.css({ transform : this.CSS_TRANSLATE_VALUE(this.stage_left + this.centering_offset) });
			}
		},

		/**
		 * カルッセルを進める
		 * @param  {Number}   d        方向と距離
		 * @param  {Function} callback コールバック
		 * @param  {String}   bezier   イージングのcubic-bezier
		 */
		step : function(d,callback,bezier){
			var A = this;
			this.available = false;
			this.move = true;
			this.autoStop();

			// 移動先
			var tl = this.stage_left - this.step_width * d;
			var n = (this.current + this.article_length + d) % this.article_length;


			if(this.IS_TRANSITION){

				// アニメーション完了後の処理
				var teFn = function(){
					// 現在位置を保存
					A.stage_left = tl;
					A.current += d;

					// 左右のダミーに到達したら元に戻す
					A.stage.css({ transition : 'none' });
					if(A.current >= A.article_length || A.current < 0){
						A.current = (A.current + A.article_length) % A.article_length;
						A.stage_left = 0 - A.ul_width - A.step_width * A.current;
						A.stage.css({ transform : A.CSS_TRANSLATE_VALUE(A.stage_left + A.centering_offset) });
					}

					A.stage.off(A.EVENT_TRANSITION_END,teFn);
					A.available = true;
					A.move = false;
					A.showIndicator();
					A.activateArticle();
					A.current = n;
					A.autoStart();
					if(!!callback) callback();
				};

				this.stage.on(this.EVENT_TRANSITION_END,teFn);
				this.stage.css({ transition : this.CSS_TRANSITION_VALUE(Math.abs(this.step_width * d * this.duration_k / 1000),(bezier||A.easing.bezier)) });
				this.stage.css({ transform : this.CSS_TRANSLATE_VALUE(tl + this.centering_offset) });
			}
			else{
				this.stage.stop().animate(
					{ left : tl + this.centering_offset },
					Math.abs(this.step_width * d * this.duration_k),
					this.easing.label,
					function(){
						A.stage_left = tl;
						A.current += d;

						if(A.current >= A.article_length || A.current < 0){
							A.current = (A.current + A.article_length) % A.article_length;
							A.stage_left = 0 - A.ul_width - A.step_width * A.current;
							A.stage.css({ left : A.stage_left + A.centering_offset });
						}

						A.available = true;
						A.move = false;
						A.showIndicator();
						A.activateArticle();
						A.current = n;
						A.autoStart();
						if(!!callback) callback();
					}
				);
			}

		},

		/**
		 * アイテムのカレント化
		 */
		activateArticle : function(){
			var A = this;

			$(this.article_class).each(function(index,item){
				var cn = item;
				var sel = (!!this.li_selector) ? this.li_selector : 'li';
				if(!!cn) sel = sel + '.' + cn;
				if(index == A.current) $(sel,A.stage).addClass('active');
				else  $(sel,A.stage).removeClass('active');
			});
		},

		/**
		 * サイズと位置調整
		 */
		adjust : function(){
			var A = this;
			this.autoStop();

			var ww = Math.min($(window).width() , this.max_width);
			var mw = (typeof this.min_width == 'function') ? this.min_width() : this.min_width;
			if(mw > ww) ww = mw;
			if(!!this.is_stable && !!this.max_width) ww = this.max_width;

			if(this.is_fullsize){
				this.step_width = this.div.width();
				if(mw > this.step_width) this.step_width = mw;
			}
			else if(!!this.step_width_fn){
				this.step_width = this.step_width_fn();
			}
			else this.step_width = this.lis.eq(0).width()
				+ parseInt(this.lis.eq(0).css('margin-left'),10)
				+ parseInt(this.lis.eq(0).css('margin-right'),10);

			this.ul_width = this.step_width * this.article_length;

			$(this.uls).each(function(){
				this.css({ width : A.ul_width });
			});
			if(this.is_fullsize) this.lis.css({ width : this.step_width });

			if(this.is_indicator){
				var indi_li = $('li',this.indi_ol);
				this.indi_step_width = indi_li.width() + parseInt(indi_li.css('paddingLeft'),10) + parseInt(indi_li.css('paddingRight'),10);
				// this.indi_ol.css({ width : this.indi_step_width * this.article_length });
				this.indi_wrap_width = this.indi_wrap.width();
			}

			this.stage.css({ width : this.ul_width * 3 });

			if(!!this.is_centering) this.centering_offset = (ww - this.step_width) / 2;
			else this.centering_offset = 0;

			this.stage_left = 0 - this.ul_width - this.current * this.step_width;

			if(this.IS_TRANSITION){
				this.stage.css({ transform : this.CSS_TRANSLATE_VALUE(this.stage_left + this.centering_offset) });
			}
			else{
				this.stage.css({ left : this.stage_left + this.centering_offset });
			}

			this.autoStart();
		},

		/**
		 * 自動カルッセル開始
		 */
		autoStart : function(){
			var A = this;

			if(!!this.is_silent) return;

			clearInterval(this.timer_id);
			this.timer_id = setInterval(function(){
				A.step(1);
			},this.auto_interval);
		},

		/**
		 * 自動カルッセル終了
		 */
		autoStop : function(){
			clearInterval(this.timer_id);
		},

		/**
		 * インジケーターの要素オブジェクト生成
		 * @param  {Number} index インデックス
		 * @param  {jQuery} li    <li>要素
		 * @return {Object}       インジケーター要素オブジェクト
		 */
		createIndiArticle : function(index,li){
			var A = this;

			var obj = {
				index : index,
				stat : 0,
				div : li,
				a : null,
				cover1 : $('<span class="cover c1"></span>'),
				cover2 : $('<span class="cover c2"></span>'),
				left : 0,
				build : function(){
					var B = this;

					this.a = $('a',this.div).off();
					this.a.append(this.cover1);
					this.a.append(this.cover2);

					var isw = this.div.width() + parseInt(this.div.css('paddingLeft'),10) + parseInt(this.div.css('paddingRight'),10);
					this.left = isw * this.index;

					this.a.on(A.CLICK_EVENT_NAME,function(evt){
						evt.preventDefault();

						if(!A.available) return;
						if(!A.indi_available) return;
						if(B.stat !== 0) return;

						A.indi_available = false;

						var current = A.current;
						var d = B.index - current;

						A.step(d,function(){
							A.indi_available = true;
						});


					});
				},
				activate : function(){
					this.stat = 1;
					this.div.addClass('active');

					var iol = 0;
					var iww = A.indi_wrap.width();
					if(this.left > iww) iol = A.indi_step_width * 3 - this.left;
					A.indi_ol.css({ left : iol });
				},
				deactivate : function(){
					this.stat = 0;
					this.div.removeClass('active');
				}
			};
			obj.build();
			return obj;
		},

		/**
		 * インジケーター要素のアクティブ化
		 */
		showIndicator : function(){
			var A = this;

			$(this.indi_article).each(function(){
				if(this.index == A.current) this.activate();
				else this.deactivate();
			});
		},

		/**
		 * 各種変数設定
		 */
		setVars : function(){
			var A = this;
			var ua = navigator.userAgent.toLowerCase();

			var IS_TRANSITION = (function(){
				var ret = (typeof $('<div>').css("transitionProperty") === "string");
				return ret;
			})();

			var CSS_PREFIX = (function(){
				var ret = '';
				if(ua.indexOf('webkit') != -1) ret = '-webkit-';
				else if(ua.indexOf('msie') != -1) ret = '-ms-';
				else if(ua.indexOf('trident') != -1) ret = '';
				else if(ua.indexOf('gecko') != -1) ret = '-moz-';
				else if(ua.indexOf('opera') != -1) ret = '-o-';

				return ret;
			})();

			var CSS_TRANSITION_VALUE = function(sec,bezier){
				return CSS_PREFIX + 'transform ' + sec + 's ' + (bezier || A.easing.bezier);
			};

			var CSS_TRANSLATE_VALUE = function(x){
				var CSS_TRANSLATE_PREFIX = (function(){
					var ret = 'translate(';
					if(ua.indexOf('webkit') != -1 || ua.indexOf('chrome') != -1) ret = 'translate3d(';
					return ret;
				})();

				var CSS_TRANSLATE_SUFFIX = (function(){
					var ret = 'px,0)';
					if(ua.indexOf('webkit') != -1 || ua.indexOf('chrome') != -1) ret = 'px,0,0)';
					return ret;
				})();

				return CSS_TRANSLATE_PREFIX + x + CSS_TRANSLATE_SUFFIX;
			}

			var EVENT_TRANSITION_END = (function(){
				var ret = 'transitionend';
				if(ua.indexOf('webkit') != -1) ret = 'webkitTransitionEnd';
				return ret;
			})();

			var CLICK_EVENT_NAME = (function(){
				if(/iphone|ipod|ipad|android/.test(ua)) return 'touchstart';
				else return 'click';
				return 'click';
			})();

			this.IS_TRANSITION = IS_TRANSITION;
			this.CSS_PREFIX = CSS_PREFIX;
			this.CSS_TRANSITION_VALUE = CSS_TRANSITION_VALUE;
			this.CSS_TRANSLATE_VALUE = CSS_TRANSLATE_VALUE;
			this.EVENT_TRANSITION_END = EVENT_TRANSITION_END;
			this.CLICK_EVENT_NAME = CLICK_EVENT_NAME;

		}
	};
	carousel.build(opt);

	return carousel;
};


SAMPLE.common.scrollJump = function(elem){
	return $(elem).each(function(){
		var a = $(this);
		var href = a.attr('href');
		var div = $(href);

		if(div.length < 1) return;

		a.on('click',function(evt){
			evt.preventDefault();
			var sh = $('.site-header');
			var offset = 0;

			if(sh.css('position') == 'fixed'){
				offset = sh.height()
					+ parseInt(sh.css('padding-top'),10)
					+ parseInt(sh.css('padding-bottom'),10)
					+ parseInt(sh.css('border-top-width'),10)
					+ parseInt(sh.css('border-bottom-width'),10)
				;
			}

			var mt = parseInt(div.css('margin-top'),10);
			if(mt < 0){
				offset += mt;
			}

			var ot = div.offset().top;

			$('html,body').stop().animate({ scrollTop : ot - offset });

		});
	});
};




// ********** ********** ********** ********** ********** ********** ********** **********
// ********** ********** ********** ********** ********** ********** ********** **********
// ********** ********** ********** ********** ********** ********** ********** **********



$(function(){

	if(SAMPLE.common.responsive.type == 'pc'){
	}

	if(SAMPLE.common.responsive.type == 'sp'){
	}
});
