<!DOCTYPE html>
<html class="home home-index" lang="ja">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
	<meta charset="utf-8">
	<title>PUGでPHPを書く方法 | サンプルサイト</title>
	<meta name="description" content="サンプルサイトです">
	<meta name="keywords" content="sample,サンプル,さんぷる">
	<meta property="og:title" />
	<meta property="og:site_name" content="サンプルサイト" />
	<meta property="og:url" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="サンプルサイトです" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="undefined | サンプルサイト" />
	<meta name="twitter:description" content="サンプルサイトです" />
	<meta name="twitter:url" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" href="/assets/img/share/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="icon" href="/assets/img/share/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="apple-touch-icon" href="/assets/img/share/favicon.png">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css?v=1.0.0">
	<link rel="stylesheet" type="text/css" href="css/standard.css?v=1.0.0">
	<script src="/assets/js/vendor/jquery.min.js"></script>
	<script src="/assets/js/common.js?v=1.0.0"></script>
	<script src="js/index.js?v=1.0.0"></script>
	<!-- Global site tag (gtag.js) - Google Analytics-->
	<script src="https://www.googletagmanager.com/gtag/js?id=UA-00000000-0" async></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		      function gtag(){dataLayer.push(arguments);}
		      gtag('js', new Date());
		      
		      gtag('config', 'UA-00000000-0');
	</script>
</head>

<body>
	<div class="wrap">
		<main>
			<h2>PUG内でPHPを書くときはこのように</h2>
			<?php echo 'hello world'; ?>
		</main>
		<footer class="site-footer">
			<div class="wrap">
				<p>copylight &copy; tyo digital works</p>
			</div>
		</footer>
	</div>
</body>

</html>