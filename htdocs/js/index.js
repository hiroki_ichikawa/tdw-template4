"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var README = function README() {
  _classCallCheck(this, README);

  console.log('hello');
  console.log(document.querySelector('.readme-md'));
  var markdown = document.querySelector('.readme-md').innerHTML;
  var html = marked(markdown);
  document.querySelector('.readme-dump').innerHTML = html;
};

window.addEventListener('DOMContentLoaded', function () {
  var readme = new README();
});
//# sourceMappingURL=index.js.map