"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var myClass =
/*#__PURE__*/
function () {
  function myClass() {
    _classCallCheck(this, myClass);

    this.name = 'digital works';
    this.age = 3;
  }

  _createClass(myClass, [{
    key: "method1",
    value: function method1() {
      var obj = {
        foo: 123,
        var: 456,
        baz: 789
      };

      for (var _i = 0, _Object$keys = Object.keys(obj); _i < _Object$keys.length; _i++) {
        var key = _Object$keys[_i];
        console.log(key, obj[key]);
      }
    }
  }, {
    key: "age",
    get: function get() {
      return this.age - 0;
    },
    set: function set(age) {
      this.age = parseInt(age);
    }
  }]);

  return myClass;
}();
//# sourceMappingURL=sample.js.map