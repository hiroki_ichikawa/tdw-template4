# TYO Digital Works 標準開発テンプレート ver.4

開発用のgulpテンプレートです
※HTMLなどの内容については特に定義されていません

## 使い方

* sourceディレクトリ内の「project_name」がひな形です
* プロジェクト名に合わせてリネームして使ってください
* gulp：gulpの設定、タスク
* src：開発用ディレクトリ。ここで書いたものがdevにビルドされます
* dev：ビルド用ディレクトリ。BrowserSyncの簡易サーバはここを参照します
* 完成したら、htdocsにコピーし、サイト毎の決まりに従ってアップロード等して公開してください


## gulpタスク

task name | file | how it works
--- | --- | ---
**default** | gulpfile.js | 1度build後、各タスクのウォッチを開始
**build** | gulpfile.js | 各タスクを1度実行。srcの内容をdevにビルド
**deploy** | gulpfile.js | minify、文字エンコード変換後、devの内容をhtdocsにコピー（file_copy）
html_copy | gulp/tasks/html.js | 画像、pug、sass、es以外のファイルをdevにコピー
html_watch | gulp/tasks/html.js | 同上ファイルの変動をウォッチしてhtml_copyを実行
image_minify | gulp/tasks/image.js | 画像ファイルのサイズを縮小してdevにコピー。縮小の設定はgulp.config.js
image_watch | gulp/tasks/image.js | 同上ファイルの変動をウォッチしてimage_minifyを実行
pug_compile | gulp/tasks/pug.js | PUGをコンパイルしてdevにコピー。phpファイルの拡張子は`.php.pug`
pug_watch | gulp/tasks/pug.js | 同上ファイルの変動をウォッチしてpug_compileを実行
sass_compile | gulp/tasks/sass.js | SASSをコンパイルしてdevにコピー。autoprefixerあり。prefixの対象は.browserlistrc
sass_watch | gulp/tasks/sass.js | 同上ファイルの変動をウォッチしてsass_compileを実行
es_transpile | gulp/tasks/scripts.js | esファイルをトランスパイルしてdevにコピー
es_watch | gulp/tasks/scripts.js | 同上ファイルの変動をウォッチしてes_transpileを実行
browser_sync | gulp/tasks/server.js | プレビュー用のwebサーバを起動。PROXYとPORTはgulp.server.jsに記述
browser_sync_reload | gulp/tasks/server.js | リロード信号をブラウザに送信
browser_sync_watch | gulp/tasks/server.js | dev以下のファイルの変動をウォッチしてbrowser_sync_reloadを実行
css_minify | gulp/tasks/deploy.js | dev以下のcssファイルをminify。設定はgulp.config.js
js_minify | gulp/tasks/deploy.js | dev以下のjsファイルをminify。設定はgulp.config.js
text_replace | gulp/tasks/deploy.js | dev以下の文字エンコード変換時に必要な文字列置換。設定はgulp.config.js
text_encoding | gulp/tasks/deploy.js | dev以下の文字エンコード変換。設定はgulp.config.js
file_copy | gulp/tasks/deploy.js | dev以下のファイルをhtdocsにコピー


## gulp.config.js

```
module.exports = {

	// directory
	path: {
		src: './src', ← 開発
		dev: './dev', ← ビルド
		deploy: '../../htdocs', ← デプロイ
		ignore_prefix: [ ← 無視用prefix
			'__'
		],
		ignore_suffix: [ ← 無視用suffix
			'__'
		]
	},

	server: {
		https: false ← HTTPSを使うかどうか
	},

	// image
	image: {
		is_minify: true, ← 圧縮の可否
		png: [0.7, 0.85], ← 圧縮レート
		jpg: 85,
		gif: 180
	},

	// deploy
	deploy: {
		encoding: {
			html: 'UTF-8', ← デプロイ時の文字エンコード
			css: 'UTF-8',
			js: 'UTF-8'
		},
		minify: {
			html: false, ← minifyの可否
			css: false,
			js: false
		},
		is_sass: false, // .sassファイルを含めるか
		is_cssmap: false, // .css.mapファイルを含めるか
		is_jsmap: false // .js.mapファイルを含めるか
	}
};
```


## gulp.server.js

```
module.exports = {
	port: 3001,
	proxy: 'localhost' ← このサーバがdevをドキュメントルートとすると都合がいいです
};
```


## 開発ログ

日付 | 内容
--- | ---
19.10.14 | gulp.watchでのsrcの指定でのpath()の利用を停止
19.10.14 | サーバの設定をgulp/gulp.server.jsに分離
19.06.28 | 初期版完成
